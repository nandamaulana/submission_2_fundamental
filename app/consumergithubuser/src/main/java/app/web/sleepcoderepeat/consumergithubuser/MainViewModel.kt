package app.web.sleepcoderepeat.consumergithubuser

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.consumergithubuser.data.entity.FavoritesEntity

class MainViewModel(
    application: Application
) :
    AndroidViewModel(application) {

    val listFavorite: ObservableList<FavoritesEntity> = ObservableArrayList()
    val isLoading = MutableLiveData<Boolean>()

    fun setListFavoriteList(list: List<FavoritesEntity>){
        listFavorite.let { 
            it.clear()
            it.addAll(list)
        }
    }

    fun setLoading(loadingState: Boolean){
        isLoading.value = loadingState
    }

}