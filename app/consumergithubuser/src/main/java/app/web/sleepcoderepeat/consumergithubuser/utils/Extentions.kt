package app.web.sleepcoderepeat.consumergithubuser.utils

import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.consumergithubuser.R
import com.bumptech.glide.Glide

fun <T: ViewModel> AppCompatActivity.obtainViewModel(viewModelCLass: Class<T>)
= ViewModelProvider(this, ViewModelFactory(application)).get(viewModelCLass)

fun ImageView.load(url: String){
    Glide.with(context)
        .load(url)
        .placeholder(R.mipmap.ic_launcher_round)
        .error(R.drawable.ic_baseline_error_24)
        .into(this)
}
