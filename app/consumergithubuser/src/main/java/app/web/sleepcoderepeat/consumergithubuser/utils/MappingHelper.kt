package app.web.sleepcoderepeat.consumergithubuser.utils

import android.database.Cursor
import android.database.MatrixCursor
import app.web.sleepcoderepeat.consumergithubuser.data.DatabaseContract.FavoritesColumn.Companion.AVATAR_URL_KEY
import app.web.sleepcoderepeat.consumergithubuser.data.DatabaseContract.FavoritesColumn.Companion.ID_FAV_KEY
import app.web.sleepcoderepeat.consumergithubuser.data.DatabaseContract.FavoritesColumn.Companion.LOGIN_KEY
import app.web.sleepcoderepeat.consumergithubuser.data.DatabaseContract.FavoritesColumn.Companion.NAME_KEY
import app.web.sleepcoderepeat.consumergithubuser.data.entity.FavoritesEntity


object MappingHelper {
    fun getCursorFromList(favorites: List<FavoritesEntity>): Cursor? {
        val cursor = MatrixCursor(
            arrayOf(
                ID_FAV_KEY, AVATAR_URL_KEY, LOGIN_KEY, NAME_KEY
            )
        )
        for (favorite in favorites) {
            cursor.newRow()
                .add(ID_FAV_KEY, favorite.id)
                .add(AVATAR_URL_KEY, favorite.avatar_url)
                .add(LOGIN_KEY, favorite.login)
                .add(NAME_KEY, favorite.name)
        }
        return cursor
    }

    fun getListFromCursor(favoritesCursor: Cursor?): List<FavoritesEntity> {
        val favList = arrayListOf<FavoritesEntity>()
        favoritesCursor?.apply {
            while (moveToNext()) {
                favList.add(
                    FavoritesEntity(
                        getInt(getColumnIndexOrThrow(ID_FAV_KEY)),
                        getString(getColumnIndexOrThrow(AVATAR_URL_KEY)),
                        getString(getColumnIndexOrThrow(LOGIN_KEY)),
                        getString(getColumnIndexOrThrow(NAME_KEY)),
                    )
                )
            }
        }
        return favList
    }
}