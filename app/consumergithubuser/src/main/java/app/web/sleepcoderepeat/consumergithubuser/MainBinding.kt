package app.web.sleepcoderepeat.consumergithubuser

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.consumergithubuser.data.entity.FavoritesEntity
import app.web.sleepcoderepeat.consumergithubuser.utils.load
import com.facebook.shimmer.ShimmerFrameLayout
import de.hdodenhof.circleimageview.CircleImageView

object MainBinding {

    @BindingAdapter("favoriteList")
    @JvmStatic
    fun setFavoriteList(recyclerView: RecyclerView, favoriteList: MutableList<FavoritesEntity>) {
        if (favoriteList.size != 0)
            (recyclerView.adapter as MainAdapter).replaceData(favoriteList)
    }
    @BindingAdapter("loadImage")
    @JvmStatic
    fun setCircleImage(circleImageView: CircleImageView, url: String) {
        if (url.isNotEmpty())
            circleImageView.load(url)
    }

    @BindingAdapter("isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading: Boolean) {
        if (isLoading) {
            shimmerFrameLayout.startShimmer()
            return
        }
        shimmerFrameLayout.stopShimmer()
    }
}