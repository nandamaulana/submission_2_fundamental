package app.web.sleepcoderepeat.consumergithubuser

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.consumergithubuser.data.entity.FavoritesEntity
import app.web.sleepcoderepeat.consumergithubuser.databinding.ListFavoriteItemBinding

class MainAdapter(
    private var favoriteList: MutableList<FavoritesEntity>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FavoriteHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_favorite_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = favoriteList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val user = favoriteList[position]
        (holder as FavoriteHolder).bindItem(user)
    }

    fun replaceData(items: MutableList<FavoritesEntity>) {
        setList(items)
    }

    private fun setList(item: MutableList<FavoritesEntity>) {
        this.favoriteList = item
        notifyDataSetChanged()
    }

    class FavoriteHolder(var binding: ListFavoriteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(favoritesEntity: FavoritesEntity) {
            binding.favorite = favoritesEntity
            binding.executePendingBindings()
        }
    }
}