package app.web.sleepcoderepeat.consumergithubuser.data

import android.net.Uri
import android.provider.BaseColumns

object DatabaseContract {

    const val AUTHORITY = "app.web.sleepcoderepeat.githubuser"
    const val SCHEME = "content"

    class FavoritesColumn : BaseColumns {

        companion object {
            const val TABLE_NAME = "favorites"
            const val ID_FAV_KEY = "id"
            const val AVATAR_URL_KEY = "avatar_url"
            const val LOGIN_KEY = "login"
            const val NAME_KEY = "name"

            val CONTENT_URI: Uri = Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build()
        }

    }
}