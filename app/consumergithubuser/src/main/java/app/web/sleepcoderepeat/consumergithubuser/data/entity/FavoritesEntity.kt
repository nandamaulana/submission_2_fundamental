package app.web.sleepcoderepeat.consumergithubuser.data.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavoritesEntity(
    val id: Int = 0,
    val avatar_url: String? = "",
    val login: String? = "",
    val name: String? = ""
) : Parcelable