package app.web.sleepcoderepeat.consumergithubuser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.web.sleepcoderepeat.consumergithubuser.data.DatabaseContract.FavoritesColumn.Companion.CONTENT_URI
import app.web.sleepcoderepeat.consumergithubuser.data.entity.FavoritesEntity
import app.web.sleepcoderepeat.consumergithubuser.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.consumergithubuser.utils.MappingHelper
import app.web.sleepcoderepeat.consumergithubuser.utils.obtainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding


    companion object {
        private const val EXTRA_STATE = "EXTRA_STATE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainBinding = ActivityMainBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(MainViewModel::class.java)
        }
        mainBinding.lifecycleOwner = this
        setContentView(mainBinding.root)
        setUpRecyclerView()

        if (savedInstanceState == null) {
            loadFavoritesAsync()
        } else {
            savedInstanceState.getParcelableArrayList<FavoritesEntity>(EXTRA_STATE)?.let {
                mainBinding.vm?.setListFavoriteList(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        loadFavoritesAsync()
    }

    private fun setUpRecyclerView() {
        mainBinding.vm?.let {
            rvFavoriteList.adapter = MainAdapter(it.listFavorite)
        }
    }

    private fun loadFavoritesAsync() {
        GlobalScope.launch(Dispatchers.Main) {
            mainBinding.vm?.let {
                it.setLoading(true)
                val deferredNotes = async(Dispatchers.IO) {
                    val cursor = contentResolver?.query(CONTENT_URI, null, null, null, null)
                    MappingHelper.getListFromCursor(cursor)
                }
                val favorites = deferredNotes.await()
                it.setLoading(false)
                if (favorites.isNotEmpty()) {
                    it.setListFavoriteList(favorites)
                } else {
                    it.setListFavoriteList(arrayListOf())
                    showSnackbarMessage("Tidak ada data saat ini")
                }

            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        mainBinding.vm?.let {
            outState.putParcelableArrayList(
                EXTRA_STATE,
                ArrayList<FavoritesEntity>(it.listFavorite)
            )
        }
    }

    private fun showSnackbarMessage(message: String) {
        Snackbar.make(mainBinding.rvFavoriteList, message, Snackbar.LENGTH_SHORT).show()
    }
}
