package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.databinding.ActivityDetailBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.favorite.FavoritesActivity
import app.web.sleepcoderepeat.githubuser.ui.screen.setting.SettingActivity
import app.web.sleepcoderepeat.githubuser.utils.Constants
import app.web.sleepcoderepeat.githubuser.utils.obtainViewModel
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.toolbar.*

class DetailActivity : AppCompatActivity() {

    private lateinit var detailBinding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailBinding = ActivityDetailBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(DetailViewModel::class.java)
        }
        detailBinding.lifecycleOwner = this
        setContentView(detailBinding.root)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(
                ContextCompat.getDrawable(
                    this@DetailActivity,
                    R.drawable.ic_baseline_chevron_left_24
                )
            )
        }

        setupFollowersFollowingAdapter()
        setupObserver()
    }

    private fun setupObserver() {
        detailBinding.vm?.apply {
            openDetailUser.observe(this@DetailActivity, {
                val intent = Intent(this@DetailActivity, DetailActivity::class.java)
                intent.putExtra(Constants.OPEN_DETAIL_USERNAME, it.username)
                val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this@DetailActivity,
                    Pair.create(it.binding.imgUser, Constants.IMG_USER_KEY),
                    Pair.create(it.binding.txtNama, Constants.USERNAME_KEY)
                ).toBundle()

                startActivity(intent, option)
            })
            message.observe(this@DetailActivity, {
                Toast.makeText(this@DetailActivity, it, Toast.LENGTH_SHORT).show()
            })
        }
    }

    private fun setupFollowersFollowingAdapter() {
        val viewModel = detailBinding.vm
        if (viewModel != null) {
            with(detailBinding) {
                vpFollowersFollowing.adapter = FollowersFollowingPagerAdapter(this@DetailActivity)
                vpFollowersFollowing.offscreenPageLimit = 2
                TabLayoutMediator(
                    tlFollowersFollowing,
                    vpFollowersFollowing
                ) { currentTab, currentPosition ->
                    currentTab.text = when (currentPosition) {
                        FollowersFollowingPagerAdapter.FOLLOWERS -> getString(R.string.followers)
                        else -> getString(R.string.following)
                    }
                }.attach()

            }

            detailBinding.vm?.init(intent.getStringExtra(Constants.OPEN_DETAIL_USERNAME))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->
                onBackPressed()
            R.id.action_change_setting -> startActivity(Intent(this, SettingActivity::class.java))
            R.id.favoriteList -> startActivity(Intent(this, FavoritesActivity::class.java))
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}