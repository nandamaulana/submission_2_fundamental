package app.web.sleepcoderepeat.githubuser.ui.screen.interfaces

interface MainActionListener {
    fun onItemClick()
}