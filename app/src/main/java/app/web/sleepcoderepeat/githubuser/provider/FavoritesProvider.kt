package app.web.sleepcoderepeat.githubuser.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import app.web.sleepcoderepeat.githubuser.data.local.DataLocalSource


class FavoritesProvider : ContentProvider() {

    private lateinit var dataLocalSource: DataLocalSource

    companion object {
        private const val FAVORITES_DATA = 1
        private const val AUTHORITY = "app.web.sleepcoderepeat.githubuser"
        private const val TABLE_NAME = "favorites"
        val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(
                AUTHORITY,
                TABLE_NAME,
                FAVORITES_DATA
            )
        }
    }

    override fun onCreate(): Boolean {
        context?.let {
            dataLocalSource = DataLocalSource(it)
        }
        return true
    }

    override fun query(
        uri: Uri,
        strings: Array<out String>?,
        s: String?,
        strings1: Array<out String>?,
        s1: String?
    ): Cursor? {
        val cursor: Cursor?
        when (uriMatcher.match(uri)) {
            FAVORITES_DATA -> {
                cursor = dataLocalSource.getCursor()
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        return cursor
    }

    override fun getType(p0: Uri): String? = null

    override fun insert(p0: Uri, p1: ContentValues?): Uri? = null

    override fun delete(p0: Uri, p1: String?, p2: Array<out String>?): Int = 0

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int = 0
}