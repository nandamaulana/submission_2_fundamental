package app.web.sleepcoderepeat.githubuser.data.local.dao

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface FavoritesDao {
    @Insert(onConflict = REPLACE)
    fun addFavorite(favorites: FavoritesEntity): Single<Long>

    @Query("SELECT * FROM favorites")
    fun getListFavorites(): Flowable<List<FavoritesEntity>>

    @Query("SELECT * FROM favorites")
    fun getListFavoritesForProvider(): Cursor

    @Query("SELECT * FROM favorites WHERE login = :login")
    fun getFavorite(login: String): Single<FavoritesEntity>

    @Query("DELETE FROM favorites WHERE id = :id")
    fun deleteFavorite(id: Int): Single<Int>
}