package app.web.sleepcoderepeat.githubuser.ui.screen.favorite

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.databinding.ActivityFavoritesBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.detail.DetailActivity
import app.web.sleepcoderepeat.githubuser.ui.screen.setting.SettingActivity
import app.web.sleepcoderepeat.githubuser.utils.Constants
import app.web.sleepcoderepeat.githubuser.utils.obtainViewModel
import kotlinx.android.synthetic.main.toolbar.*

class FavoritesActivity : AppCompatActivity() {

    private lateinit var favoritesBinding: ActivityFavoritesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        favoritesBinding = ActivityFavoritesBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(FavoriteViewModel::class.java)
        }
        favoritesBinding.lifecycleOwner = this
        setContentView(favoritesBinding.root)
        //perlu cast karena include, View Found while Toolbar Expected
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_baseline_chevron_left_24
            )
        )

        setUpObserver()
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        favoritesBinding.vm?.let {
            favoritesBinding.rvFavoriteList.adapter =
                FavoriteAdapter(it.listFavorite, it)
        }
    }

    private fun setUpObserver() {
        favoritesBinding.vm?.apply {
            openDetailUser.observe(this@FavoritesActivity, {
                val intent = Intent(this@FavoritesActivity, DetailActivity::class.java)
                intent.putExtra(Constants.OPEN_DETAIL_USERNAME, it.username)
                val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this@FavoritesActivity,
                    Pair.create(it.binding.imgUser, Constants.IMG_USER_KEY),
                    Pair.create(it.binding.txtNama, Constants.USERNAME_KEY)
                ).toBundle()

                startActivity(intent, option)
            })
            message.observe(this@FavoritesActivity, {
                Toast.makeText(this@FavoritesActivity, it, Toast.LENGTH_SHORT).show()
            })
        }
        favoritesBinding.vm?.init()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        menu?.findItem(R.id.favoriteList)?.isVisible = false
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_change_setting -> startActivity(Intent(this, SettingActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}