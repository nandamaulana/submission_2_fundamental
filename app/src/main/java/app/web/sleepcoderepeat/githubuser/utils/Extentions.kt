package app.web.sleepcoderepeat.githubuser.utils

import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.data.remote.DataRemoteSource
import com.bumptech.glide.Glide
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.floor

fun <T: ViewModel> AppCompatActivity.obtainViewModel(viewModelCLass: Class<T>)
= ViewModelProvider(this, ViewModelFactory(application,DataRemoteSource())).get(viewModelCLass)

fun ImageView.load(url: String){
    Glide.with(context)
        .load(url)
        .placeholder(R.mipmap.ic_launcher_round)
        .error(R.drawable.ic_baseline_error_24)
        .into(this)
}

fun Int.toKFormat() : String {
    var value = this.toString()
    if (this >=1000){
        value = "${floor((this/1000).toDouble()).toInt()}K"
    }
    return value
}

fun String.isValidDate(format: String = "yyyy-MM-dd"): Boolean {
    return try {
        val df = SimpleDateFormat(format, Locale.getDefault())
        df.isLenient = false
        df.parse(this)
        true
    } catch (e: ParseException) {
        false
    }
}

fun String.toCalendar(): Calendar {
    return this.let {
        val timeArray = this.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        Calendar.getInstance().apply {
        this.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]))
        this.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]))
        this.set(Calendar.SECOND, 0)
        }
    }
}