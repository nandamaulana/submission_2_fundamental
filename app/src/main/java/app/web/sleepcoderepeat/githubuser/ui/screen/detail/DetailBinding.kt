package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.api.dataclass.User

object DetailBinding {
    @BindingAdapter("listFollowersFollowing")
    @JvmStatic
    fun setListFollowers(recyclerView: RecyclerView, followerList: MutableList<User>){
        if (followerList.size != 0)
            (recyclerView.adapter as FollowersFollowingListAdapter).replaceData(followerList)
    }
}