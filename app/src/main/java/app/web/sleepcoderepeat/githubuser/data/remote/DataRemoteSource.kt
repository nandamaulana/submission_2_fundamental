package app.web.sleepcoderepeat.githubuser.data.remote

import android.annotation.SuppressLint
import app.web.sleepcoderepeat.githubuser.api.ApiService
import app.web.sleepcoderepeat.githubuser.data.MainDataRemoteSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DataRemoteSource : MainDataRemoteSource {
    private val apiService = ApiService.create()

    @SuppressLint("CheckResult")
    override fun getSearchUserData(
        callback: MainDataRemoteSource.GetSearchUserCallback,
        userName: String?
    ) {
        if (!userName.isNullOrBlank()) {
            apiService.getSearchUser(userName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it != null)
                        callback.onDataLoaded(it)
                    else
                        callback.onNotAvailable()
                }, {
                    callback.onError(it.localizedMessage)
                })
        }
    }

    @SuppressLint("CheckResult")
    override fun getUserDetailData(
        callback: MainDataRemoteSource.GetUserDetailCallback,
        userName: String?
    ) {
        if (!userName.isNullOrBlank()) {
            apiService.getUserDetail(userName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it != null)
                        callback.onDataLoaded(it)
                    else
                        callback.onNotAvailable()
                }, {
                    callback.onError(it.localizedMessage)
                })
        }
    }

    @SuppressLint("CheckResult")
    override fun getFollowerUserData(
        callback: MainDataRemoteSource.GetFollowerUserCallback,
        userName: String?
    ) {
        if (!userName.isNullOrBlank()) {
            apiService.getFollowerUser(userName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it != null)
                        callback.onDataLoaded(it)
                    else
                        callback.onNotAvailable()
                }, {
                    callback.onError(it.localizedMessage)
                })
        }
    }

    @SuppressLint("CheckResult")
    override fun getFollowingUserData(
        callback: MainDataRemoteSource.GetFollowingUserCallback,
        userName: String?
    ) {
        if (!userName.isNullOrBlank()){
        apiService.getFollowingUser(userName)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it != null)
                    callback.onDataLoaded(it)
                else
                    callback.onNotAvailable()
            }, {
                callback.onError(it.localizedMessage)
            })
        }
    }

}