package app.web.sleepcoderepeat.githubuser.application

import android.app.AlarmManager
import android.app.Notification.EXTRA_TITLE
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.Toast
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.utils.isValidDate
import app.web.sleepcoderepeat.githubuser.utils.toCalendar

class AlarmReceiver : BroadcastReceiver() {

    companion object {
        val notificationUtils = NotificationUtils()
        const val ID_REPEATING = 101
    }

    override fun onReceive(context: Context, intent: Intent) {
        notificationUtils.showAlarmNotification(
            context,
            intent.getStringExtra(EXTRA_TITLE) ?: "",
            intent.getStringExtra(EXTRA_MESSAGE) ?: "",
            ID_REPEATING
        )
    }

    fun setRepeatingAlarm(context: Context, time: String, title: String, message: String) {

        if (!time.isValidDate("HH:mm")) return
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, AlarmReceiver::class.java).apply {
            putExtra(EXTRA_MESSAGE, message)
            putExtra(EXTRA_TITLE, title)
        }
        val pendingIntent = PendingIntent.getBroadcast(context, ID_REPEATING, intent, 0)
        alarmManager.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            time.toCalendar().timeInMillis,
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )

        Toast.makeText(context, context.getString(R.string.alarm_active, time), Toast.LENGTH_SHORT)
            .show()
    }

    fun cancelAlarm(context: Context, idType: Int) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, idType, intent, 0)
        pendingIntent.cancel()

        alarmManager.cancel(pendingIntent)

        Toast.makeText(context, context.getString(R.string.alarm_nonactive), Toast.LENGTH_SHORT)
            .show()
    }
}
