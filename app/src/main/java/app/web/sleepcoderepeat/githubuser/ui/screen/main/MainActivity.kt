package app.web.sleepcoderepeat.githubuser.ui.screen.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.detail.DetailActivity
import app.web.sleepcoderepeat.githubuser.ui.screen.favorite.FavoritesActivity
import app.web.sleepcoderepeat.githubuser.ui.screen.setting.SettingActivity
import app.web.sleepcoderepeat.githubuser.utils.Constants
import app.web.sleepcoderepeat.githubuser.utils.obtainViewModel
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding
    private val mHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(MainViewModel::class.java)
        }
        mainBinding.lifecycleOwner = this
        setContentView(mainBinding.root)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setUpObserver()
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        mainBinding.vm?.let {
            mainBinding.rvSearchResult.adapter = MainUserAdapter(it.listUser, it)
        }
    }

    private fun setUpObserver() {
        mainBinding.vm?.apply {
            openDetailUser.observe(this@MainActivity, {
                val intent = Intent(this@MainActivity, DetailActivity::class.java)
                intent.putExtra(Constants.OPEN_DETAIL_USERNAME, it.username)
                val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    this@MainActivity,
                    Pair.create(it.binding.imgUser, Constants.IMG_USER_KEY),
                    Pair.create(it.binding.txtNama, Constants.USERNAME_KEY)
                ).toBundle()

                startActivity(intent, option)
            })
            keyword.observe(this@MainActivity, {
                mHandler.removeCallbacksAndMessages(null)
                mHandler.postDelayed({
                    mainBinding.vm?.getListUser()
                }, 500)
            })
            message.observe(this@MainActivity, {
                Toast.makeText(this@MainActivity, it, Toast.LENGTH_SHORT).show()
            })
        }
        mainBinding.vm?.init()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_change_setting -> startActivity(Intent(this, SettingActivity::class.java))
            R.id.favoriteList -> startActivity(Intent(this, FavoritesActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}