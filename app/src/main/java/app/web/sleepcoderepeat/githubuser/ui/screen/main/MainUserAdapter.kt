package app.web.sleepcoderepeat.githubuser.ui.screen.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.databinding.ListUserItemBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.interfaces.MainActionListener

class MainUserAdapter(
    private var userList: MutableList<User>,
    private var mainViewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MainUserHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_user_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val user = userList[position]
        val actionListener = object :
            MainActionListener {
            override fun onItemClick() {
                mainViewModel.openDetail((holder as MainUserHolder).binding, user.login)
            }
        }
        (holder as MainUserHolder).bindItem(user, actionListener)
    }

    fun replaceData(items: MutableList<User>) {
        setList(items)
    }

    private fun setList(item: MutableList<User>) {
        this.userList = item
        notifyDataSetChanged()
    }

    class MainUserHolder(var binding: ListUserItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(user: User, listener: MainActionListener) {
            binding.user = user
            binding.action = listener
            binding.executePendingBindings()
        }
    }
}