package app.web.sleepcoderepeat.githubuser.ui.screen.favorite

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.data.MainDataLocalSource
import app.web.sleepcoderepeat.githubuser.data.local.DataLocalSource
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import app.web.sleepcoderepeat.githubuser.databinding.ListFavoriteItemBinding
import app.web.sleepcoderepeat.githubuser.utils.SingleLiveEvent

class FavoriteViewModel(
    application: Application
) :
    AndroidViewModel(application) {

    private lateinit var dataLocalSource: DataLocalSource

    val listFavorite: ObservableList<FavoritesEntity> = ObservableArrayList()
    val isLoading = MutableLiveData<Boolean>()
    val message = MutableLiveData<String>()
    private val getApplication = getApplication<Application>()
    internal val openDetailUser = SingleLiveEvent<FavoriteClickObject>()

    fun init() {
        isLoading.value = false
        dataLocalSource = DataLocalSource(this.getApplication.applicationContext)
        getListFavorite()
    }

    fun openDetail(binding: ListFavoriteItemBinding, userName: String) {
        val tempData = FavoriteClickObject(binding, userName)
        openDetailUser.value = tempData
    }

    fun getListFavorite() {
        isLoading.value = true
        dataLocalSource.getAll(object : MainDataLocalSource.GetAllFavoritesCallback {
            override fun onSuccess(list: List<FavoritesEntity>) {
                with(listFavorite) {
                    clear()
                    addAll(list)
                }
                isLoading.value = false
            }

            override fun onError(msg: String?) {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }

            override fun onNotAvailable() {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }
        })
    }
}