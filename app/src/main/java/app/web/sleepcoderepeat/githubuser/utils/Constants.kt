package app.web.sleepcoderepeat.githubuser.utils

object Constants {
    const val BASE_URL = "https://api.github.com/"

    const val OPEN_DETAIL_USERNAME = "userName"

    const val IMG_USER_KEY = "img_user"

    const val USERNAME_KEY = "user_name"

    const val TYPE_KEY = "type"

}