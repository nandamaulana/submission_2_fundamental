package app.web.sleepcoderepeat.githubuser.ui.screen.favorite

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity

object FavoriteBinding {

    @BindingAdapter("favoriteList")
    @JvmStatic
    fun setFavoriteList(recyclerView: RecyclerView, favoriteList: MutableList<FavoritesEntity>) {
        if (favoriteList.size != 0)
            (recyclerView.adapter as FavoriteAdapter).replaceData(favoriteList)
    }
}