package app.web.sleepcoderepeat.githubuser.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import app.web.sleepcoderepeat.githubuser.data.local.dao.FavoritesDao
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity


@Database(
    entities = [FavoritesEntity::class],
    version = 1,
    exportSchema = false
)
abstract class GithubLocalDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "githublocal.db"
        private var INSTANCE: GithubLocalDatabase? = null
        private fun create(context: Context): GithubLocalDatabase =
            Room.databaseBuilder(context, GithubLocalDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()

        fun getInstance(context: Context): GithubLocalDatabase =
            (INSTANCE ?: synchronized(this) {
                INSTANCE ?: create(context).also { INSTANCE = it }
            })
    }

    abstract fun favoritesDao(): FavoritesDao
}