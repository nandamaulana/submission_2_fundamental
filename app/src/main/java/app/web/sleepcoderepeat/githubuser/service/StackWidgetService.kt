package app.web.sleepcoderepeat.githubuser.service

import android.content.Intent
import android.widget.RemoteViewsService
import app.web.sleepcoderepeat.githubuser.ui.widget.StackRemoteViewFactory

class StackWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory =
        StackRemoteViewFactory(this.applicationContext)
}