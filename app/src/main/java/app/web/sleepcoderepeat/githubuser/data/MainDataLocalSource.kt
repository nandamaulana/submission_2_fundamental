package app.web.sleepcoderepeat.githubuser.data

import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity

interface MainDataLocalSource {
    fun add(favoritesEntity: FavoritesEntity?, callback: AddFavoritesCallback)
    fun getAll(callback: GetAllFavoritesCallback)
    fun show(login: String?, callback: ShowFavoriteCallback)
    fun remove(id: Int?, callback: RemoveFavoritesCallback)

    interface AddFavoritesCallback{
        fun onSuccess(id: Long?)
        fun onError(msg: String?)
        fun onNotAvailable()
    }

    interface GetAllFavoritesCallback{
        fun onSuccess(list: List<FavoritesEntity>)
        fun onError(msg: String?)
        fun onNotAvailable()
    }

    interface ShowFavoriteCallback{
        fun onSuccess(favoritesEntity: FavoritesEntity?)
        fun onError(msg: String?)
        fun onNotAvailable()
    }

    interface RemoveFavoritesCallback{
        fun onSuccess(id: Int?)
        fun onError(msg: String?)
        fun onNotAvailable()
    }
}