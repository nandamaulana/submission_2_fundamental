package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.api.response.DetailUserResponse
import app.web.sleepcoderepeat.githubuser.data.MainDataLocalSource
import app.web.sleepcoderepeat.githubuser.data.MainDataRemoteSource
import app.web.sleepcoderepeat.githubuser.data.local.DataLocalSource
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import app.web.sleepcoderepeat.githubuser.data.remote.DataRemoteSource
import app.web.sleepcoderepeat.githubuser.databinding.ListUserItemBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.main.UserClickObject
import app.web.sleepcoderepeat.githubuser.utils.SingleLiveEvent
import app.web.sleepcoderepeat.githubuser.utils.toKFormat

class DetailViewModel(
    application: Application,
    private val dataRemoteRemoteSource: DataRemoteSource
) :
    AndroidViewModel(application) {

    private lateinit var dataLocalSource: DataLocalSource

    val detailUserDataField: ObservableField<DetailUserResponse> = ObservableField()
    val listFollowers: ObservableList<User> = ObservableArrayList()
    val listFollowing: ObservableList<User> = ObservableArrayList()
    val isLoading = MutableLiveData<Boolean>()
    val message = MutableLiveData<String>()
    val txtFollower = MutableLiveData<String>()
    val txtRepository = MutableLiveData<String>()
    private val getApplication = getApplication<Application>()
    internal val openDetailUser = SingleLiveEvent<UserClickObject>()
    val favoriteStatus = MutableLiveData<Boolean>()
    val idFavorite = MutableLiveData<Int>()

    private fun getTxtFollower(followers: Int, following: Int): String {
        return getApplication.resources.getString(
            R.string.following_followers_total,
            followers.toKFormat(),
            following.toKFormat()
        )
    }

    private fun getTxtRepository(repository: Int): String {
        return getApplication.resources.getString(
            R.string.repository_total, repository.toKFormat()
        )
    }

    fun init(userName: String?) {
        isLoading.value = true
        favoriteStatus.value = false
        idFavorite.value = null
        if (!userName.isNullOrBlank()) {
            dataLocalSource = DataLocalSource(this.getApplication.applicationContext)
            getDetailUser(userName)
            getListFollowers(userName)
            getListFollowing(userName)
            getFavoriteStatus(userName)
        }
    }

    fun openDetail(binding: ListUserItemBinding, userName: String) {
        val tempData = UserClickObject(binding, userName)
        openDetailUser.value = tempData
    }

    fun favoriteClick(login: String) {
        if (favoriteStatus.value == false) {
            val favoritesEntity = FavoritesEntity(
                name = detailUserDataField.get()?.name,
                avatar_url = detailUserDataField.get()?.avatar_url,
                login = login
            )
            dataLocalSource.add(
                favoritesEntity,
                object : MainDataLocalSource.AddFavoritesCallback {
                    override fun onSuccess(id: Long?) {
                        getFavoriteStatus(login)
                    }

                    override fun onError(msg: String?) {
                        getFavoriteStatus(login)
                        message.value =
                            getApplication<Application>().resources.getString(R.string.data_not_available)
                    }

                    override fun onNotAvailable() {
                        getFavoriteStatus(login)
                        message.value =
                            getApplication<Application>().resources.getString(R.string.data_not_available)
                    }
                })
        } else {
            dataLocalSource.remove(
                idFavorite.value,
                object : MainDataLocalSource.RemoveFavoritesCallback {
                    override fun onSuccess(id: Int?) {
                        idFavorite.value = null
                        getFavoriteStatus(login)
                    }

                    override fun onError(msg: String?) {
                        message.value =
                            getApplication<Application>().resources.getString(R.string.data_not_available)
                        getFavoriteStatus(login)
                    }

                    override fun onNotAvailable() {
                        getFavoriteStatus(login)
                    }
                })
        }
    }

    private fun getFavoriteStatus(userName: String) {
        dataLocalSource.show(userName, object : MainDataLocalSource.ShowFavoriteCallback {
            override fun onSuccess(favoritesEntity: FavoritesEntity?) {
                favoriteStatus.value = favoritesEntity != null
                idFavorite.value = null
                if (favoritesEntity != null)
                    idFavorite.value = favoritesEntity.id
            }

            override fun onError(msg: String?) {
                favoriteStatus.value = false
                idFavorite.value = null
            }

            override fun onNotAvailable() {
                favoriteStatus.value = false
                idFavorite.value = null
            }
        })
    }

    private fun getDetailUser(userName: String) {
        dataRemoteRemoteSource.getUserDetailData(object :
            MainDataRemoteSource.GetUserDetailCallback {
            override fun onDataLoaded(detailUserResponse: DetailUserResponse) {
                detailUserDataField.set(detailUserResponse)
                isLoading.value = false
                txtRepository.value = getTxtRepository(detailUserResponse.public_repos)
                txtFollower.value =
                    getTxtFollower(detailUserResponse.followers, detailUserResponse.followers)
            }

            override fun onNotAvailable() {
                txtFollower.value = getTxtFollower(0, 0)
                isLoading.value = false
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
            }

            override fun onError(msg: String?) {
                txtFollower.value = getTxtFollower(0, 0)
                isLoading.value = false
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
            }
        }, userName)
    }

    private fun getListFollowers(userName: String) {
        dataRemoteRemoteSource.getFollowerUserData(object :
            MainDataRemoteSource.GetFollowerUserCallback {
            override fun onDataLoaded(followerList: MutableList<User>) {
                with(listFollowers) {
                    clear()
                    addAll(followerList)
                }
                isLoading.value = false
            }

            override fun onNotAvailable() {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }

            override fun onError(msg: String?) {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }
        }, userName)
    }

    private fun getListFollowing(userName: String) {
        dataRemoteRemoteSource.getFollowingUserData(object :
            MainDataRemoteSource.GetFollowingUserCallback {
            override fun onDataLoaded(followingList: MutableList<User>) {
                with(listFollowing) {
                    clear()
                    addAll(followingList)
                }
                isLoading.value = false
            }

            override fun onNotAvailable() {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }

            override fun onError(msg: String?) {
                message.value =
                    getApplication<Application>().resources.getString(R.string.data_not_available)
                isLoading.value = false
            }
        }, userName)
    }
}