package app.web.sleepcoderepeat.githubuser.utils

import android.database.Cursor
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity


object MappingHelper {
    private const val ID_FAV_KEY = "id"
    private const val AVATAR_URL_KEY = "avatar_url"
    private const val LOGIN_KEY = "login"
    private const val NAME_KEY = "name"

    fun getListFromCursor(favoritesCursor: Cursor?): List<FavoritesEntity> {
        val favList = arrayListOf<FavoritesEntity>()
        favoritesCursor?.apply {
            while (moveToNext()) {
                favList.add(
                    FavoritesEntity(
                        getInt(getColumnIndexOrThrow(ID_FAV_KEY)),
                        getString(getColumnIndexOrThrow(AVATAR_URL_KEY)),
                        getString(getColumnIndexOrThrow(LOGIN_KEY)),
                        getString(getColumnIndexOrThrow(NAME_KEY)),
                    )
                )
            }
        }
        return favList
    }
}