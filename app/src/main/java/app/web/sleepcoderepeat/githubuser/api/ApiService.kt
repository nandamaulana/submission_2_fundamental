package app.web.sleepcoderepeat.githubuser.api

import app.web.sleepcoderepeat.githubuser.BuildConfig
import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.api.response.DetailUserResponse
import app.web.sleepcoderepeat.githubuser.api.response.SearchResponse
import app.web.sleepcoderepeat.githubuser.utils.Constants
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("search/users")
    @Headers("Authorization: token ${BuildConfig.API_KEY}")
    fun getSearchUser(
        @Query("q") q: String
    ): Observable<SearchResponse>

    @GET("users/{userName}")
    @Headers("Authorization: token ${BuildConfig.API_KEY}")
    fun getUserDetail(
        @Path("userName") userName: String
    ): Observable<DetailUserResponse>

    @GET("users/{userName}/followers")
    @Headers("Authorization: token ${BuildConfig.API_KEY}")
    fun getFollowerUser(
        @Path("userName") userName: String
    ): Observable<MutableList<User>>

    @GET("users/{userName}/following")
    @Headers("Authorization: token ${BuildConfig.API_KEY}")
    fun getFollowingUser(
        @Path("userName") userName: String
    ): Observable<MutableList<User>>

    companion object Factory {
        fun create(): ApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client =
                OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .client(client)
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(Constants.BASE_URL)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}