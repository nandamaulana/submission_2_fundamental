package app.web.sleepcoderepeat.githubuser.api.response

import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import java.io.Serializable

data class SearchResponse(
    val total_count: Int,
    val incomplete_results: Boolean,
    val items: MutableList<User>
) : Serializable