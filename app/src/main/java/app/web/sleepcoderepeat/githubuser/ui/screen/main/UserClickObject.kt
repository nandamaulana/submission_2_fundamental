package app.web.sleepcoderepeat.githubuser.ui.screen.main

import app.web.sleepcoderepeat.githubuser.databinding.ListUserItemBinding

data class UserClickObject(var binding: ListUserItemBinding, var username: String)