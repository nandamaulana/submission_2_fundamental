package app.web.sleepcoderepeat.githubuser.data

import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.api.response.DetailUserResponse
import app.web.sleepcoderepeat.githubuser.api.response.SearchResponse

interface MainDataRemoteSource {
    fun getSearchUserData(callback: GetSearchUserCallback, userName: String?)
    fun getUserDetailData(callback: GetUserDetailCallback, userName: String?)
    fun getFollowerUserData(callback: GetFollowerUserCallback, userName: String?)
    fun getFollowingUserData(callback: GetFollowingUserCallback, userName: String?)

    interface GetSearchUserCallback {
        fun onDataLoaded(searchResponse: SearchResponse)
        fun onNotAvailable()
        fun onError(msg: String?)
    }
    interface GetUserDetailCallback {
        fun onDataLoaded(detailUserResponse: DetailUserResponse)
        fun onNotAvailable()
        fun onError(msg: String?)
    }
    interface GetFollowerUserCallback {
        fun onDataLoaded(followerList: MutableList<User>)
        fun onNotAvailable()
        fun onError(msg: String?)
    }
    interface GetFollowingUserCallback {
        fun onDataLoaded(followingList: MutableList<User>)
        fun onNotAvailable()
        fun onError(msg: String?)
    }
}