package app.web.sleepcoderepeat.githubuser.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Entity(tableName = "favorites")
@Parcelize
data class FavoritesEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "avatar_url") val avatar_url: String? = "",
    @ColumnInfo(name = "login") val login: String? = "",
    @ColumnInfo(name = "name") val name: String? = ""
) : Serializable, Parcelable