package app.web.sleepcoderepeat.githubuser.utils

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.githubuser.data.remote.DataRemoteSource
import app.web.sleepcoderepeat.githubuser.ui.screen.detail.DetailViewModel
import app.web.sleepcoderepeat.githubuser.ui.screen.favorite.FavoriteViewModel
import app.web.sleepcoderepeat.githubuser.ui.screen.main.MainViewModel

class ViewModelFactory(
        private val application: Application,
        private val dataRemoteRemoteSource: DataRemoteSource
) : ViewModelProvider.NewInstanceFactory(){

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>)= with(modelClass) {
        when{
            isAssignableFrom(MainViewModel::class.java) ->
                MainViewModel(application,dataRemoteRemoteSource)
            isAssignableFrom(DetailViewModel::class.java) ->
                DetailViewModel(application,dataRemoteRemoteSource)
            isAssignableFrom(FavoriteViewModel::class.java) ->
                FavoriteViewModel(application)
            else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        }
    }as T

}