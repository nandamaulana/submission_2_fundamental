package app.web.sleepcoderepeat.githubuser.ui.screen.main

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.api.dataclass.User

object MainBinding {

    @BindingAdapter("userList")
    @JvmStatic
    fun setUserList(recyclerView: RecyclerView, userList: MutableList<User>) {
        if (userList.size != 0)
            (recyclerView.adapter as MainUserAdapter).replaceData(userList)
    }
}