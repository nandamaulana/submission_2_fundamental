package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.databinding.ListUserItemBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.interfaces.MainActionListener

class FollowersFollowingListAdapter(
    private var followersFollowingAdapter: MutableList<User>,
    private var detailViewModel: DetailViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FollowerHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_user_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = followersFollowingAdapter.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val user = followersFollowingAdapter[position]
        val actionListener = object :
            MainActionListener {
            override fun onItemClick() {
                detailViewModel.openDetail((holder as FollowerHolder).binding, user.login)
            }
        }
        (holder as FollowerHolder).bindItem(user, actionListener)
    }

    fun replaceData(items: MutableList<User>) {
        setList(items)
    }

    private fun setList(item: MutableList<User>) {
        this.followersFollowingAdapter = item
        notifyDataSetChanged()
    }

    class FollowerHolder(var binding: ListUserItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(user: User, listener: MainActionListener) {
            binding.user = user
            binding.action = listener
            binding.executePendingBindings()
        }
    }
}