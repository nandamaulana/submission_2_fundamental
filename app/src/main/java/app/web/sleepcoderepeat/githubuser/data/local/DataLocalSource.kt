package app.web.sleepcoderepeat.githubuser.data.local

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import app.web.sleepcoderepeat.githubuser.data.MainDataLocalSource
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DataLocalSource(context: Context) : MainDataLocalSource {

    private val favoritesDao = GithubLocalDatabase.getInstance(context).favoritesDao()

    override fun add(
        favoritesEntity: FavoritesEntity?,
        callback: MainDataLocalSource.AddFavoritesCallback
    ) {
        favoritesEntity?.let {
            favoritesDao.addFavorite(it)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t: Long? ->
                    if (t != null)
                        callback.onSuccess(t)
                    else
                        callback.onNotAvailable()
                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                }
                )
        }
    }

    @SuppressLint("CheckResult")
    override fun getAll(callback: MainDataLocalSource.GetAllFavoritesCallback) {
        favoritesDao.getListFavorites()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ t: List<FavoritesEntity>? ->
                if (t != null)
                    callback.onSuccess(t)
                else
                    callback.onNotAvailable()
            }, { t: Throwable? ->
                callback.onError(t?.localizedMessage)
            })
    }

    override fun remove(
        id: Int?,
        callback: MainDataLocalSource.RemoveFavoritesCallback
    ) {
        id?.let {
            favoritesDao.deleteFavorite(it)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t ->
                    if (t != null)
                        callback.onSuccess(t)
                    else
                        callback.onNotAvailable()

                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                })
        }
    }

    override fun show(
        login: String?,
        callback: MainDataLocalSource.ShowFavoriteCallback
    ) {
        login?.let {
            favoritesDao.getFavorite(it)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t ->
                    if (t != null)
                        callback.onSuccess(t)
                    else
                        callback.onNotAvailable()
                }, { t: Throwable? ->
                    callback.onError(t?.localizedMessage)
                })
        }
    }

    fun getCursor(): Cursor {
        return favoritesDao.getListFavoritesForProvider()
    }
}