package app.web.sleepcoderepeat.githubuser.ui.screen.setting

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.application.AlarmReceiver
import app.web.sleepcoderepeat.githubuser.application.AlarmReceiver.Companion.ID_REPEATING

class SettingPreferenceFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var reminderKey: String
    private lateinit var languageKey: String

    private lateinit var reminderPreference: SwitchPreference
    private lateinit var languagePreference: Preference

    private lateinit var alarmReceiver: AlarmReceiver

    companion object {
        private const val time = "01:00"
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        init()
    }

    private fun init() {
        reminderKey = resources.getString(R.string.key_reminder)
        languageKey = resources.getString(R.string.key_language)

        reminderPreference = findPreference<SwitchPreference>(reminderKey) as SwitchPreference
        languagePreference = findPreference<Preference>(languageKey) as Preference
        languagePreference.setOnPreferenceClickListener {
            startActivity(Intent(Settings.ACTION_LOCALE_SETTINGS))
            false
        }
        alarmReceiver = AlarmReceiver()
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
        when (key) {
            reminderKey -> changeStateReminder(sharedPreferences)
        }
    }

    private fun changeStateReminder(sharedPreferences: SharedPreferences) {
        val reminderState = sharedPreferences.getBoolean(
            reminderKey,
            false
        )
        reminderPreference.isChecked = reminderState

        context?.let {
            if (reminderState)
                alarmReceiver.setRepeatingAlarm(
                    it,
                    time,
                    resources.getString(R.string.back_to_app_title),
                    resources.getString(R.string.back_to_app_message)
                )
            else
                alarmReceiver.cancelAlarm(
                    it,
                    ID_REPEATING
                )
        }
    }

}