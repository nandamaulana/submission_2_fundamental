package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class FollowersFollowingPagerAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {

    override fun createFragment(position: Int): Fragment {
        return when(position){
            FOLLOWERS -> FollowersFollowingFragment.getInstance(FOLLOWERS)
            else -> FollowersFollowingFragment.getInstance(FOLLOWING)
        }
    }

    override fun getItemCount(): Int = TABS.size


    companion object {
        const val FOLLOWERS = 0
        const val FOLLOWING = 1
        val TABS = arrayOf(FOLLOWERS, FOLLOWING)
    }
}