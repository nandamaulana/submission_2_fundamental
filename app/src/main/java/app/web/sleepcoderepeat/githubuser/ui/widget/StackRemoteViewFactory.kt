package app.web.sleepcoderepeat.githubuser.ui.widget

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.os.bundleOf
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.data.local.DataLocalSource
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import app.web.sleepcoderepeat.githubuser.utils.MappingHelper.getListFromCursor
import com.bumptech.glide.Glide




class StackRemoteViewFactory(private val mContext: Context): RemoteViewsService.RemoteViewsFactory {

    private var dataLocalSource = DataLocalSource(mContext)

    private val mWidgetItem = ArrayList<FavoritesEntity>()

    override fun onCreate() {
    }

    override fun onDataSetChanged() {
        mWidgetItem.addAll(getListFromCursor(dataLocalSource.getCursor()))
    }

    override fun onDestroy() {
    }

    override fun getCount(): Int = mWidgetItem.size

    override fun getViewAt(position: Int): RemoteViews {
        val remoteView = RemoteViews(mContext.packageName, R.layout.widget_item)
        val data = mWidgetItem[position]
        var bitmap = BitmapFactory.decodeResource(mContext.resources, R.drawable.loading)
        data.avatar_url?.let {
            bitmap = getBitmap(it)
        }
        var textNama = ""
        data.name?.let {
            textNama = it
        }
        remoteView.setImageViewBitmap(R.id.img_user, bitmap)
        remoteView.setTextViewText(R.id.txt_nama, textNama)

        val extras = bundleOf(
            FavoritesStackedWidget.EXTRA_ITEM to textNama
        )

        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)

        remoteView.setOnClickFillInIntent(R.id.card_view, fillInIntent)
        return remoteView
    }

    private fun getBitmap(url: String): Bitmap{
        Log.e("url",url)
        return Glide.with(mContext)
            .asBitmap()
            .load(url)
            .submit(512, 512)
            .get()
    }

    override fun getLoadingView(): RemoteViews = RemoteViews(mContext.packageName, R.layout.widget_loading)

    override fun getViewTypeCount(): Int = 1

    override fun getItemId(p0: Int): Long = 0

    override fun hasStableIds(): Boolean = false
}