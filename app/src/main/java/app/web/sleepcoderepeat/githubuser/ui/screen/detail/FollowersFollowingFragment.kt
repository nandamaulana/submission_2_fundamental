package app.web.sleepcoderepeat.githubuser.ui.screen.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.githubuser.databinding.FragmentFollowersBinding
import app.web.sleepcoderepeat.githubuser.databinding.FragmentFollowingBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.detail.FollowersFollowingPagerAdapter.Companion.FOLLOWERS
import app.web.sleepcoderepeat.githubuser.utils.Constants.TYPE_KEY
import app.web.sleepcoderepeat.githubuser.utils.obtainViewModel

class FollowersFollowingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewModel = (activity as DetailActivity).obtainViewModel(DetailViewModel::class.java)
        val detailBinding = if (arguments?.getInt(TYPE_KEY) == FOLLOWERS)
            FragmentFollowersBinding.inflate(inflater).apply {
                vm = viewModel
            }
        else
            FragmentFollowingBinding.inflate(inflater).apply {
                vm = viewModel
            }

        return detailBinding.let {
            if (arguments?.getInt(TYPE_KEY) == FOLLOWERS) {
                val viewModelFollowers = (it as FragmentFollowersBinding).vm
                if (viewModelFollowers != null)
                    it.rvFollowers.adapter =
                        FollowersFollowingListAdapter(
                            viewModelFollowers.listFollowers,
                            viewModelFollowers
                        )
                it.lifecycleOwner = this
            } else {
                val viewModelFollowing = (it as FragmentFollowingBinding).vm
                if (viewModelFollowing != null)
                    it.rvFollowing.adapter =
                        FollowersFollowingListAdapter(
                            viewModelFollowing.listFollowing,
                            viewModelFollowing
                        )
                it.lifecycleOwner = this
            }
            it.root
        }
    }

    companion object {
        fun getInstance(type: Int): Fragment {
            val fragment = FollowersFollowingFragment()
            val args = Bundle()
            args.putInt(TYPE_KEY, type)
            fragment.arguments = args
            return fragment
        }
    }

}