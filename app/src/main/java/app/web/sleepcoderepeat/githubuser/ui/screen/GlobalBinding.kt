package app.web.sleepcoderepeat.githubuser.ui.screen

import androidx.databinding.BindingAdapter
import app.web.sleepcoderepeat.githubuser.utils.load
import com.facebook.shimmer.ShimmerFrameLayout
import de.hdodenhof.circleimageview.CircleImageView

object GlobalBinding {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun setCircleImage(circleImageView: CircleImageView, url: String) {
        if (url.isNotEmpty())
            circleImageView.load(url)
    }

    @BindingAdapter("isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading: Boolean) {
        if (isLoading) {
            shimmerFrameLayout.startShimmer()
            return
        }
        shimmerFrameLayout.stopShimmer()
    }

}