package app.web.sleepcoderepeat.githubuser.ui.screen.favorite

import app.web.sleepcoderepeat.githubuser.databinding.ListFavoriteItemBinding

data class FavoriteClickObject(var binding: ListFavoriteItemBinding, var username: String)