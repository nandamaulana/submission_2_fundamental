package app.web.sleepcoderepeat.githubuser.ui.screen.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.data.local.entity.FavoritesEntity
import app.web.sleepcoderepeat.githubuser.databinding.ListFavoriteItemBinding
import app.web.sleepcoderepeat.githubuser.ui.screen.interfaces.MainActionListener

class FavoriteAdapter(
    private var favoriteList: MutableList<FavoritesEntity>,
    private var favoriteViewModel: FavoriteViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FavoriteHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_favorite_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = favoriteList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val user = favoriteList[position]
        val actionListener = object :
            MainActionListener {
            override fun onItemClick() {
                user.login?.let {
                    favoriteViewModel.openDetail((holder as FavoriteHolder).binding, it)
                }
            }
        }
        (holder as FavoriteHolder).bindItem(user, actionListener)
    }

    fun replaceData(items: MutableList<FavoritesEntity>) {
        setList(items)
    }

    private fun setList(item: MutableList<FavoritesEntity>) {
        this.favoriteList = item
        notifyDataSetChanged()
    }

    class FavoriteHolder(var binding: ListFavoriteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(favoritesEntity: FavoritesEntity, listener: MainActionListener) {
            binding.favorite = favoritesEntity
            binding.action = listener
            binding.executePendingBindings()
        }
    }
}