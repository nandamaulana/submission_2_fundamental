package app.web.sleepcoderepeat.githubuser.ui.screen.main

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.githubuser.R
import app.web.sleepcoderepeat.githubuser.api.dataclass.User
import app.web.sleepcoderepeat.githubuser.api.response.SearchResponse
import app.web.sleepcoderepeat.githubuser.data.MainDataRemoteSource
import app.web.sleepcoderepeat.githubuser.data.remote.DataRemoteSource
import app.web.sleepcoderepeat.githubuser.databinding.ListUserItemBinding
import app.web.sleepcoderepeat.githubuser.utils.SingleLiveEvent

class MainViewModel(
    application: Application,
    private val dataRemoteRemoteSource: DataRemoteSource
) :
    AndroidViewModel(application) {

    val listUser: ObservableList<User> = ObservableArrayList()
    val showResultCount = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()
    val keyword = MutableLiveData<String>()
    val message = MutableLiveData<String>()
    private val getApplication = getApplication<Application>()
    internal val openDetailUser = SingleLiveEvent<UserClickObject>()

    fun getResultCountFormatted(value: Int): String {
        return getApplication.resources.getQuantityString(
            R.plurals.numberUserFound, value, value
        )
    }

    fun init() {
        isLoading.value = false
        showResultCount.value = getResultCountFormatted(0)
    }

    fun openDetail(binding: ListUserItemBinding, userName: String) {
        val tempData = UserClickObject(binding, userName)
        openDetailUser.value = tempData
    }

    fun getListUser() {
        if (!keyword.value.isNullOrBlank()) {

            isLoading.value = true
            dataRemoteRemoteSource.getSearchUserData(object :
                MainDataRemoteSource.GetSearchUserCallback {
                override fun onDataLoaded(searchResponse: SearchResponse) {
                    with(listUser) {
                        clear()
                        addAll(searchResponse.items)
                    }
                    isLoading.value = false
                    showResultCount.value = getResultCountFormatted(listUser.size)
                }

                override fun onNotAvailable() {
                    message.value =
                        getApplication<Application>().resources.getString(R.string.data_not_available)
                    isLoading.value = false
                    showResultCount.value = getResultCountFormatted(0)
                }

                override fun onError(msg: String?) {
                    message.value =
                        getApplication.resources.getString(R.string.make_sure_have_connection)
                    isLoading.value = false
                    showResultCount.value = getResultCountFormatted(0)
                }
            }, keyword.value)
        }
    }
}